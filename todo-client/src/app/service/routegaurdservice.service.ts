import { Injectable } from '@angular/core';
import { HardcodeAuthService } from './hardcode-auth.service';

import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoutegaurdserviceService implements CanActivate{

  constructor(private hardcodeAuthService:HardcodeAuthService,private router: Router) { }

  canActivate(route:ActivatedRouteSnapshot ,state: RouterStateSnapshot)
  {
     if(this.hardcodeAuthService.isLoggedIn())
        return true;
        else
        {
          this.router.navigate(['login']);
          false;
        }
       
  }
}
