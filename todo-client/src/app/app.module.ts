import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpDataService } from './service/http-data.service';
import { WelcomeComponent } from './component/welcome/welcome.component';
import { LoginComponent } from './component/login/login.component';
import { ErrorComponent } from './component/error/error.component';
import { TaskListComponent } from './component/task-list/task-list.component';
import { MenuComponent } from './component/menu/menu.component';
import { FooterComponent } from './component/footer/footer.component';
import { LogoutComponent } from './component/logout/logout.component';
import { RegisterationFormComponent } from './component/registeration-form/registeration-form.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginNewComponent } from './component/login-new/login-new.component';

@NgModule({
  declarations: [ 
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    ErrorComponent,
    TaskListComponent,
    MenuComponent,
    FooterComponent,
    LogoutComponent,
    RegisterationFormComponent,
    LoginNewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [HttpDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
