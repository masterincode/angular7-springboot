import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { WelcomeComponent } from './component/welcome/welcome.component';
import { RoutegaurdserviceService } from './service/routegaurdservice.service';
import { TaskListComponent } from './component/task-list/task-list.component';
import { LogoutComponent } from './component/logout/logout.component';
import { RegisterationFormComponent } from './component/registeration-form/registeration-form.component';
import { LoginNewComponent } from './component/login-new/login-new.component';



const routes: Routes = [  
  
  { path:'login' , component: LoginComponent},
  { path:'welcome/:name' , component: WelcomeComponent , canActivate:[RoutegaurdserviceService] },
  { path:'task' , component: TaskListComponent , canActivate:[RoutegaurdserviceService]  },
  { path:'logout' , component: LogoutComponent , canActivate:[RoutegaurdserviceService] },
  { path:'register' , component: RegisterationFormComponent , canActivate:[RoutegaurdserviceService] },
  { path: '**', redirectTo: 'index' },
  { path: 'index', component: LoginComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
