import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HardcodeAuthService } from 'src/app/service/hardcode-auth.service';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router:Router,private hardcodeAuthService:HardcodeAuthService) { }

  ngOnInit()
   {
      this.hardcodeAuthService.logout();
  }

  loginForm()
  {
    this.router.navigate(['login']);
  }

}
