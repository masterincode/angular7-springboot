import { Component, OnInit } from '@angular/core';
import { HardcodeAuthService } from 'src/app/service/hardcode-auth.service';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
}) 
export class MenuComponent implements OnInit {

  showHide:boolean=false;

  constructor(private hardcodeAuthService : HardcodeAuthService) { }

  ngOnInit()
   {
      this.showHide=this.hardcodeAuthService.isLoggedIn();
      console.log('--this.showHide---'+this.showHide);
    }

    
  }
