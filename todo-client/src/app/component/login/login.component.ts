import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HardcodeAuthService } from 'src/app/service/hardcode-auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit
 {
   username: string="Balmukund";
   password: string='Asdf1234';
   loginForm: Object=
   {
    //  username :'',
    //  password :'',
     city : '' ,
     state: 'Maharshtra',
     age: 28
   };

   invalidLogin:boolean=false;
 

  constructor(private router: Router,private hardcodeAuthService : HardcodeAuthService) { }

  ngOnInit()
   {
    
  }

  submitForm(): void
  {
    if(this.hardcodeAuthService.authentication(this.username ,this.password)) 
   {
     this.invalidLogin=false
     this.router.navigate(['welcome',this.username])
   }
   else
   {
     this.invalidLogin=true;
    //  this.router.navigate(['error'])
    //  window.location.reload();
   }
  }

}
