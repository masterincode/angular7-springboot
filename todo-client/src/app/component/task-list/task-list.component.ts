import { Component, OnInit } from '@angular/core';
import { HttpDataService } from 'src/app/service/http-data.service';

export class TaskList 
{
    constructor(
      public id:number,
      public assignTask:string,
      public isDone:boolean,
      public targetDate: Date
    ){}

}
@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})


export class TaskListComponent implements OnInit
{
   t1:TaskList = new TaskList(1,'Develop Project',false,new Date());
   t2:TaskList = new TaskList(2,'SIT',false,new Date());
   t3:TaskList = new TaskList(3,'End to End Testing',false,new Date());
   t4:TaskList = new TaskList(4,'UAT deployemnt',false,new Date());

  task=[this.t1,this.t2,this.t3,this.t4];

  constructor(private http:HttpDataService) { }
  taskListObject:any=[]

  header:any=
  {
    username:null
  }
 
 

  ngOnInit() 
  {
      this.header.username=sessionStorage.getItem('validUserName');
      console.log('--fetchTodoList--username--',this.header);

    this.http.post('fetchTodoList',this.header).subscribe(
      (res)=>{console.log('--fetchTodoList---',res);
         this.taskListObject = res.data.fetchTodoList;
      },
      (err)=>{},
      ()=>{}
      );
  }

  deleteData(id:any)
  {
    this.http.post('deleteData',id).subscribe(
      (res)=>{console.log('--deleteData---',id);
         this.taskListObject = res
         window.location.reload();
      },
      (err)=>{},
      ()=>{}
      );
  }

}
