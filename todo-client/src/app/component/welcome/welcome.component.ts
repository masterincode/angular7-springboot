import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WelcomeDataService } from 'src/app/service/welcome-data.service';
import { HttpDataService } from 'src/app/service/http-data.service';

//in Spring 
//ComponenetScan( value="controller.package.name")
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})

//public class Welcome implements OnInit
//NOte: export keyword is used in order to access this ts file from other ts file.

export class WelcomeComponent implements OnInit
{

// Welcome(){}   default constructore 
  constructor(private activatedRoute: ActivatedRoute, private welcomeDataService:WelcomeDataService ,private http:HttpDataService) { }

    username:string
    todoListForm : any={}
  
  // void ngOnInit(){}
  ngOnInit() : void 
  {
        this.username=this.activatedRoute.snapshot.params['username']
        console.log('--username--',this.username);
  }

  getListObject()
  {
      console.log(this.welcomeDataService.executeHelloWorldService().subscribe());
      this.welcomeDataService.executeHelloWorldService().subscribe(
        response=>this.handleResponse(response)
      );
      console.log(' after suscribe--');
  }

  handleResponse(response)
  {
     
  } 

   submitForm()
   {
     console.log('--this.todoListForm--',this.todoListForm);
    this.http.post('submitToDoList',this.todoListForm).subscribe(
      (res)=>{console.log(res);},
      (err)=>{},
      ()=>{}
      );

   }

}