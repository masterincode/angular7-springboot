import { Component, OnInit } from '@angular/core';
import { HttpDataService } from 'src/app/service/http-data.service';


@Component({
  selector: 'app-registeration-form',
  templateUrl: './registeration-form.component.html',
  styleUrls: ['./registeration-form.component.css']
})
export class RegisterationFormComponent implements OnInit {

  constructor(private http:HttpDataService) { }

  registerationForm:any={}

  ngOnInit()
  {
    this.http.post('fetchData',{}).subscribe(
      (res)=>{console.log(res);
        this.registerationForm = res
      },
      (err)=>{},
      ()=>{}
      );;
  }


  submitForm(){
    console.log('submit called  ');
    this.http.post('submitData',this.registerationForm).subscribe(
      (res)=>{console.log(res);},
      (err)=>{},
      ()=>{}
      );

  }

}
