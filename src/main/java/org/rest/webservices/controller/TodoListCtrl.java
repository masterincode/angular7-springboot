package org.rest.webservices.controller;

import java.util.HashMap;

import org.rest.webservices.dto.TodoListDto;
import org.rest.webservices.model.TodoModel;
import org.rest.webservices.response.ResponseObject;
import org.rest.webservices.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Component
// @CrossOrigin(origins="http://localhost:4200")
@RestController
public class TodoListCtrl {
	
	@Autowired
	private TodoListService todoListService;

	HashMap<String, Object> object = new HashMap<String, Object>();

	@PostMapping("/fetchTodoList")
	public ResponseObject fetchTodoList(@RequestBody TodoModel model) {
		System.out.println("--in ctrl--fetchTodoList-" + model);
		return todoListService.findByUsername(model);
	}

	@PostMapping("/submitToDoList")
	public HashMap<String,Object> submitToDoList(@RequestBody TodoListDto dto) {

		System.out.println("-submitToDoList  ctrl---" + dto.toString());
		try {
			todoListService.saveData(dto);
			object.put("result", "SUCCESS");

		} catch (Exception e) {
			object.put("result", "FAILURE");
		}
		return object;
	}

	@PostMapping("/deleteData")
	public void deleteData(@RequestBody Long id) {
		System.out.println("-submitToDoList  ctrl---" + id);
		todoListService.deleteData(id);
	}

	public HashMap<String, Object> getObject() {
		return object;
	}

}
