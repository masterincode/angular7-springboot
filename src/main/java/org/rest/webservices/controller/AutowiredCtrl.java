package org.rest.webservices.controller;

import org.assertj.core.util.Arrays;
import org.rest.webservices.service.FixedDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AutowiredCtrl {

	@Autowired
	private FixedDepositService FixedDepositService;
	
	@ModelAttribute
	public void addAttributes(Model model){
		
		String[] arr= {"Mumbai","Banglore","Chennai","Kolkatta"};
		model.addAttribute("cityList", Arrays.asList(arr));
	}
	
	
	@GetMapping("deposit")
	public double getFixedDeposit(){
		return FixedDepositService.calculateFixedDeposit();
	}
}
