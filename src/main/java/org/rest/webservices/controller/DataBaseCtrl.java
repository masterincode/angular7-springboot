package org.rest.webservices.controller;

import javax.validation.Valid;

import org.rest.webservices.dto.DataTableDto;
import org.rest.webservices.response.ResponseObject;
import org.rest.webservices.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataBaseCtrl
{

	@Autowired
	private HelloWorldService service;
	
//	@Autowired
//	private MessageSource messagesource;
		
	@RequestMapping(path="/findCity/{id}")
	public ResponseObject findCity(@PathVariable Long id) 
	{
		return service.findCity(id);
	}
	
	@GetMapping("findAll")
	public ResponseObject findAll()
	{
		return service.findAll();
	}
	
	
	/* this method is for filtering data*/
	@GetMapping("findAllDistrict") 
	public MappingJacksonValue findAllDistrict()
	{
		return service.findAllDistrict();
	}
	
	
	@RequestMapping("/findCityUsingDto")
	public ResponseObject findCityUsingDto(@RequestBody DataTableDto dto)
	{
		return service.findCity(dto.getId());
	}
	
	@RequestMapping("/save")
	public ResponseObject save(@Valid @RequestBody DataTableDto dto)
	{
		return service.save(dto);
	}
	
//	@RequestMapping("/internationalalized")
//	public String internationalalized(@RequestHeader(name="Accept-Language")Locale  locale){
//		return messagesource.getMessage("good.morning.message",null ,locale);
//	}
	
	
	
}
