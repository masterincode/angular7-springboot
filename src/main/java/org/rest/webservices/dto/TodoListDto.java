package org.rest.webservices.dto;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TODO_LIST")
public class TodoListDto implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(name="todo_id")
	private long id;
	
	@Column(name="USERNAME",length=30,nullable=true)
	private String username;
	
	@Column(name="ASSIGNTASK",length=30,nullable=true)
	private String assignTask;
	
	@Column(name="TARGETDATE",length=30,nullable=true)
	private String targetDate;
	
	@Column(name="ISDONE",length=30,nullable=true)
	private String isDone;
	
//	@OneToMany(cascade = CascadeType.ALL,mappedBy="todolistdto")
//	private List<UserJobs> userJobs;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAssignTask() {
		return assignTask;
	}

	public void setAssignTask(String assignTask) {
		this.assignTask = assignTask;
	}

	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	public String getIsDone() {
		return isDone;
	}

	public void setIsDone(String isDone) {
		this.isDone = isDone;
	}

//	public List<UserJobs> getUserJobs() {
//		return userJobs;
//	}
//
//	public void setUserJobs(List<UserJobs> userJobs) {
//		this.userJobs = userJobs;
//	}

	public TodoListDto() {
		super();
	}

	@Override
	public String toString() {
		return "TodoListDto [id=" + id + ", username=" + username + ", assignTask=" + assignTask + ", targetDate="
				+ targetDate + ", isDone=" + isDone + "]";
	}

//	@Override
//	public String toString() {
//		return "TodoListDto [id=" + id + ", username=" + username + ", assignTask=" + assignTask + ", targetDate="
//				+ targetDate + ", isDone=" + isDone + ", userJobs=" + userJobs + "]";
//	}
	
	


}
