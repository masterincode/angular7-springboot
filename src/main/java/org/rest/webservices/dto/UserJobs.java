package org.rest.webservices.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="USER_JOBS")
public class UserJobs {
	
	@Id
	@GeneratedValue
	@Column(name="JOB_NO")
	private Long jobNo;
	private String jobType;
	private String location;
	
	@ManyToOne
	@JoinColumn(name="todo_id")
	private TodoListDto todolistdto;

	public Long getJobNo() {
		return jobNo;
	}

	public void setJobNo(Long jobNo) {
		this.jobNo = jobNo;
	}
	
	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public TodoListDto getTodoListDto() {
		return todolistdto;
	}

	public void setTodoListDto(TodoListDto todolistdto) {
		this.todolistdto = todolistdto;
	}

	@Override
	public String toString() {
		return "UserJobs [jobNo=" + jobNo + ", jobType=" + jobType + ", location=" + location + "]";
	}

	
}
