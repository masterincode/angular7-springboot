package org.rest.webservices.service;


import javax.transaction.Transactional;

import org.rest.webservices.dto.TodoListDto;
import org.rest.webservices.model.TodoModel;
import org.rest.webservices.repository.TodoListRepo;
import org.rest.webservices.response.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoListService {
	@Autowired
	private TodoListRepo todoListRepo;

	@Autowired
	private ResponseObject response;
	
	@Transactional
	public ResponseObject findByUsername(TodoModel model) {
		response.addData("fetchTodoList", todoListRepo.findByUsername(model.getUsername()));
		return response;
	}
	
	@Transactional
	public void deleteData(Long id) {
		todoListRepo.deleteById(id);
	}

	@Transactional
	public void saveData(TodoListDto dto) {
		System.out.println("--while saving--" + dto.toString());
		todoListRepo.save(dto);
	}

	

}
