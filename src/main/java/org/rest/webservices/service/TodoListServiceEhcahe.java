package org.rest.webservices.service;


import javax.transaction.Transactional;

import org.rest.webservices.dto.TodoListDto;
import org.rest.webservices.model.TodoModel;
import org.rest.webservices.repository.TodoListRepo;
import org.rest.webservices.response.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class TodoListServiceEhcahe {
	@Autowired
	private TodoListRepo todoListRepo;

	@Autowired
	private ResponseObject response;
	
	/*@Cacheable will be executed only once for the given cachekey and subsequent requests won't execute the method,
	 *  until the cache expires or gets flushed.
	 * */
	@Transactional
	@Cacheable(value="angularCache",key="#model")
	public ResponseObject findByUsername(TodoModel model) {
		response.addData("Data count", todoListRepo.count());
		response.addData("fetchTodoList", todoListRepo.findByUsername(model.getUsername()));
		System.out.println("---service data--222-------------------------------------------------");
		return response;
	}
	
	
	/*When u will delete data from database @CacheEvict will delete that same id
	 *from cache by searching key="#id"
	 */
	@Transactional
	@CacheEvict(value="angularCache",key="#id")
	public void deleteData(Long id) {
		todoListRepo.deleteById(id);
	}
	
	/* @Cacheput will only update the values that are stale and hence it calls the method every time to update the cache.
	 * */

	@Transactional
	@CachePut(value="angularCache",key="#dto")
	public void saveData(TodoListDto dto) {
		System.out.println("--while saving--" + dto.toString());
		todoListRepo.save(dto);
	}

	

}
