package org.rest.webservices.service;

import org.rest.webservices.repository.FixedDepositInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FixedDepositService {

	@Autowired
	private FixedDepositInterface BOBfixedDepositImpl;
	
	@Autowired
	private FixedDepositInterface deposit;
	
	
//	public FixedDepositService(FixedDepositInterface fixedDepositInterface) {
//		super();
//		this.fixedDepositInterface = fixedDepositInterface;
//	}

	public double calculateFixedDeposit(){
		
		double amount=deposit.getFixedDepositInterest();
		System.out.println("----amount---"+amount);
		return amount;
	}

	
}
