package org.rest.webservices.implementation;

import org.rest.webservices.repository.FixedDepositInterface;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class SBIfixedDepositImpl implements FixedDepositInterface{

	@Override
	public double getFixedDepositInterest() {
		// TODO Auto-generated method stub
		return 9.5;
	}

}
