package org.rest.webservices.repository;

import org.rest.webservices.dto.DataTableDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class SpringJDBC {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void databaseOperation(DataTableDto dto){
		
		jdbcTemplate.update("update city_1to99 set name=?,countryCode=? , district=?  where id=1",dto.getName(),dto.getCountryCode(),dto.getDistrict());
		
	}

}
