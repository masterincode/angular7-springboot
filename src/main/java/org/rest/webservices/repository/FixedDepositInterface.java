package org.rest.webservices.repository;

import org.springframework.stereotype.Component;

@Component
public interface FixedDepositInterface {

	public double getFixedDepositInterest();
}
