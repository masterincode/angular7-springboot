package org.rest.webservices.repository;

import java.util.List;

import org.rest.webservices.dto.TodoListDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoListRepo extends JpaRepository<TodoListDto,Long>
{
//	@Query(value = "select * from todo_list where username = ?1", nativeQuery = true)
//	List<TodoListDto> findByUsername(String username);
	
	List<TodoListDto> findByUsername(String username);

}
