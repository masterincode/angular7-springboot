package org.rest.webservices.repository;

import org.rest.webservices.dto.DataTableDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataTableRepo extends CrudRepository<DataTableDto,Long> { 
	
}
