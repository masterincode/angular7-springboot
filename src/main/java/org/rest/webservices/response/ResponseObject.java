package org.rest.webservices.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class ResponseObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, Object> data;
	
	public void addData(String key, Object value) 
	{
		System.out.println("--in response----"+data);
		if (data == null)
			data = new HashMap<String, Object>();
		    data.put(key, value);
		    System.out.println("--data size----"+data.size());
	}
	
	// GETTERS
//		@JsonProperty
		public Map<String, Object> getData() {
			return data;
		}
		
		@Override
		public String toString() {
			return String.format("{data=%s}",data);
		}
		

}
