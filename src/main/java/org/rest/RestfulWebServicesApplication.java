package org.rest;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@SpringBootApplication
@EnableCaching
public class RestfulWebServicesApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(RestfulWebServicesApplication.class, args);
	}
	
	// For running application in other than embeded web container like Jboss,
		// Wildfly, Tomcat
		@Override
		protected SpringApplicationBuilder configure(SpringApplicationBuilder application) { 
			return application.sources(applicationClass);
		}

		private static Class<RestfulWebServicesApplication> applicationClass = RestfulWebServicesApplication.class;
		
		@Bean
		public LocaleResolver localeResolver(){
			SessionLocaleResolver localeResolver = new SessionLocaleResolver();
			localeResolver.setDefaultLocale(Locale.US);
			return localeResolver;
		}
		
		@Bean
		public ResourceBundleMessageSource  bundleMessageSource(){
			ResourceBundleMessageSource messagesource= new ResourceBundleMessageSource();
			messagesource.setBasename("message");
			return messagesource;
			
		}

}
